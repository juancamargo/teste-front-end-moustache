$( document ).ready(function() {

  $(".moustache-tel-mask").mask("(99) 9 9999-9999");
  $(".moustache-birth-mask").mask('00/00/0000');
  $('.moustache-cep-mask').mask('00000-000');
  
  $('#formulario').validate({
    rules: {
        email: {
          required: true,
          email: true
        },
        nome: {
          required: true,
          minlength: 2
        },
        logradouro: {
          required: true,
          minlength: 2
        },
        numero: {
          required: true,
          minlength: 1,
          maxlength: 6,
          digits: true
        },
        tel: {
          required: true,
          minlength: 15,
          maxlength: 16,
        },
        birth: {
          required: true,
          minlength: 10,
          maxlength: 10,
        },
        cep: {
          required: true,
          maxlength: 9,
        },
        bairro: {
          required: true,
          minlength: 5
        },
        cidade: {
          required: true,
          minlength: 5
        },
        estado: {
          required: true,
          minlength: 2
        }
    }
  });

  $('.slider').slick({
      dots: false,
      infinite: true,
      arrows: false,
      autoplay: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplaySpeed: 9000
  });

  $('.sliderArticles').slick({
      dots: true,
      infinite: true,
      autoplay: false,
      speed: 500,
      slidesToShow: 3,
      slidesToScroll: 3,
      autoplaySpeed: 9000,
      prevArrow: $('.prev'),
      nextArrow: $('.next'),
      appendDots: $('.dots'),
      responsive: [
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
  });
});
